module.exports = {
	preset: 'jest-puppeteer',
	testRegex: './*\\.test\\.js$',
	globalSetup: './setUpPuppeteer.js',
	globalTeardown: './tearDownPuppeteer.js',
	testEnvironment: './customNodeEnvironment.js',
}
